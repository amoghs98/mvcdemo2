﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCDemo.Models
{
    public class CompanyModel
    {
        static List<EmployeeModel> Employee ;

        
        
        public static List<EmployeeModel> GetInstance()
        {
            if(Employee == null)
            {
                Employee = new List<EmployeeModel>();
                EmployeeModel a1 = new EmployeeModel { EmpID = "A1", Name = "Amogh", EmailID = "amogh@gmail.com", Role = "Developer", Contact = 9768091704 };
                EmployeeModel a2 = new EmployeeModel { EmpID = "A2", Name = "Test1", EmailID = "test1@gmail.com", Role = "Tester", Contact = 8988693639 };
                EmployeeModel a3 = new EmployeeModel { EmpID = "A3", Name = "test2", EmailID = "test2@gmail.com", Role = "Terter", Contact = 8452179652 };
                Employee.Add(a1);
                Employee.Add(a2);
                Employee.Add(a3);
            }
            return Employee;
        }


    }
}