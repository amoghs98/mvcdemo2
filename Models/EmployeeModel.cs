﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCDemo.Models
{
    public class EmployeeModel
    {
        [Required(ErrorMessage ="Employee ID Required")]
        [StringLength(3,ErrorMessage ="Please Enter Valid Employee Id Eg:A01")]
        public string EmpID { get; set; }

        [Display(Name = "Employee Name")]
        [Required(ErrorMessage = "Name Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "EmailID Required")]
        [EmailAddress]
        public string EmailID { get; set; }

        [Required(ErrorMessage = "Role Required")]
        public string Role { get; set; }

        [Required(ErrorMessage = "Contact Required")]
        [Phone]
        public long Contact { get; set; }
    }
}