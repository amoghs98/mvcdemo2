﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCDemo.Models;

namespace MVCDemo.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult ShowDetails()
        {
            List<EmployeeModel> model = CompanyModel.GetInstance();
            return View(model);
        }

        public ActionResult Create()
        {
            ViewData["Head"] = "NewEmployee";
            var model = new CompanyModel();
            return View(model);
        }
        public ActionResult Edit( EmployeeModel model)
        {
            ViewData["Head"] = "EditEmployee";
            return View("Create", model);
        }
        
        public ActionResult NewEmployee(EmployeeModel item)
        {
            var model = CompanyModel.GetInstance();
            if (ModelState.IsValid)
            {
            Boolean flag = false;
            foreach (var m in model)
            {
                if (m.EmpID == item.EmpID)
                    flag = true;
            }
            if (flag == true)
            {
                ViewData["Error"] = "Entered Email Already Exists";
                return View("Edit", item);
            }
            model.Add(item);
            }
            return RedirectToAction("ShowDetails");
        }

        public ActionResult EditEmployee()
        {

            return RedirectToAction("ShowDetails");
        }
    }
}